#!/usr/db/bin/perl -w
#
# tested on SUSE   11.4 dbkdlin02
#           REDHAT 7.9  dbkdprov01
# ------------------------------------------------------------------------- #
#
#   RCM - Reliable Configuration Management
#
#   (C) Copyright Kyndryl 2022
#   All Rights Reserved.
#
# ------------------------------------------------------------------------- #
#   Version:    0.1
#
#   Module:     hosts_allow.pl
#
#   Author:     Robert Rotter
#
#   Date:       10.06.2022
#
#   Purpose:
#       grab all records related to hosts_allow file
#                  ALL:    localhost
#                  sshd:   1234.rze.de.db.com
#
#   Assumptions:
#       we always expect that $service, $function does never contain a ':'
#
# ------------------------------------------------------------------------- #
use strict;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use feature qw(say);
use Architecture;
use Machine;
use Query;
use Record;
use Data::Dumper;
use File::Compare qw(compare);
use ConfigFile;
use Text::Diff;
use File::Copy;
use POSIX qw(strftime);


#-----find is running SSHD daemon with PID file---------------------------------#
my @args = ('cat /var/run/sshd.pid');
  system(@args) == 0
  or die "system @args failed: $?";
print "\nNice SSH PID found moving forward\n";

#-------------------------START--------------------------------------------------#
#DELETE UNVANTED FILE FROM WORKING DIRECTORY
#to be decided
my $file = "/home/xtrnrps/perl_test/test_income.txt";
unlink $file;

if ( -e $file ) {
    print "File still exists!\n";
}
else {
    print "File gone.\n";
}

#------QUERRY START AND CREATE COPY IN TMP ---#
#RETRIEVE THE HOSTS_FILE  AND WRITE TO TEMP FILE
#demo file is src1 = test_income.txt

my $src1   = '/home/xtrnrps/perl_test/test_income.txt';
my $des1   = '/home/xtrnrps/perl_test/test_in_place.txt';

open( DATA, ">$src1" );
print DATA"
#############################################
#
# /etc/hosts.allow  -  TCP Wrapper config file
#
# Created automatically at $(date +'%Y-%m-%d %H:%M')
#
# Do not modify: Changes will be lost !!!
#
ALL:    localhost\n";

while (<DATA>)
 {
    print DATA $_;

 }
close(DATA);

my $hostid = Machine->hostid;
my $key = 'tcpd_perm_by_hostid';
my $version = '1.0';
my $query = Query->new($key, $version);
my $results = $query->query( { hostid => $hostid } );

foreach ($results->records)
{
my $rec = Record->new($_);

if ( open( my $FH, ">>", "$src1" ) || die("error: $!") )
   {
     print $FH $rec->getval('service') . ":\t" . $rec->getval('client') . "\n";
     close($FH);
   }
}

close(DATA);
print "\nGreat ! New Source File Created\n";

#-----Replace ssh with sshd----#

my $filename = $src1 ;

my $data = read_file($filename);
   $data =~ s/ssh/sshd/g;

 write_file($filename, $data);

 sub read_file
   {
    my ($filename) = @_;

    open my $in, '<:encoding(UTF-8)', $filename or die "Could not open '$filename' for reading $!";
    local $/ = undef;
    my $all = <$in>;
    close $in;
    return $all;
  }

 sub write_file
  {
    my ($filename, $content) = @_;

    open my $out, '>:encoding(UTF-8)', $filename or die "Could not open '$filename' for writing $!";;
    print $out $content;
    close $out;
    return;
  }
print "\nExcelent ! SSH replced as SSHD\n";
#--------------Retrieve te Host_allow ----#
#Retrive the local copy of /etc/hosts_allow file at working Directory
my $dest_tmp_file = '/home/xtrnrps/perl_test/destination_tmp_file.txt';
  if ( system("cat $src1 >$dest_tmp_file") == 0 )
  {
  print "\nFinal Income file copyed to Destination_tmp\n";
  }

my $date_tag = strftime( "_%d-%m-%Y-%H-%M", localtime );
my $src2     = '/home/xtrnrps/perl_test/test_in_place.txt';
my $bkp      = '/home/xtrnrps/perl_test/destination_tmp_file.txt';

if ( copy( "$src2", "$bkp$date_tag" ) == 0 )    # die ("\nCopy failed\n");
{
    print "\nNo worries Backup file success based on current date\n";
}

#--------------DIFF-----------#
#CHECK DIFF AND WRITE TO DIFF-FILE
#diff is based on income file created src1 test_income and etc_host(test file is test_in_place des1)
#diff file is $diff - diff_previous
#if files are equal script will die
#my $des1   = '/home/xtrnrps/perl_test/test_in_place.txt';

my $diff = '/home/xtrnrps/perl_test/diff_previous.txt';
my $rc   = system("diff -u  $src1 $des1 >> $diff");

if ( compare( "$src1", "$des1" ) == 0 )
  {
    print "\nOoops ! They're equal , script will Quit\n";
    die;
  }

#-----------COPY ORIGINAL AND MAKE BACKUP with DATE---#
#original file is the test one des1=etc_host(test file is test_in_place)
#backup created incremental based on current DATE

my $date_tag = strftime( "_%d-%m-%Y-%H-%M", localtime );
my $des2     = '/home/xtrnrps/perl_test/backup_copy_of_des1.txt';

if ( system("cat $des1 > $des2$date_tag") == 0 )
  {
    print "\nNo worries Backup file success based on current date\n";
  }

#-----------CREATE HOST_ALLOW IN /TMP ---------#
# Opening file in append mode using >> #
#$dest_tmp_file  will be the final file of $src1 , prepaded to replace the original ETC_HOSTS..ALLOW

#my $dest_tmp_file = '/home/xtrnrps/perl_test/destination_tmp_file.txt';

if ( system("cat $src1 >$dest_tmp_file") == 0 )
  {
   print "\nFinal Income file copyed to Destination_tmp\n";
  }
#------REPLACE WORKING FILE WITH NEW/TMP FILE--------------#
#this steep wil move the temporary file to original place(here is for test purpose $final_dest_file)

my $final_dest_file = '/home/xtrnrps/perl_test/final_dest_file.txt';

if ( system("cat $dest_tmp_file > $final_dest_file") == 0 )
  {
    print "\nAmazing ! Replacement of ETC_HOSTS.allow SUCCESS !!\n";
    print "\nGood Bye\n";
  }

exit;

############
# ------------------------------------------------------------------------- #
#  SUB usage
# ------------------------------------------------------------------------- #
sub usage {
    my $message = shift;

    print STDERR $0 . ": error: " . $message . "\n" if ( defined($message) );

    print STDERR <<EOF
  Usage: $0 [RCM Options] [--id Service-IDs] [more Options]

  Options:
    --id <service-id>       clone the listed service-ids (E.g. s1,s2,s3)
    --mode                  [insert|select] (Def: select)
    --unique                remove duplicate data
    --format <format>       output format: stanza, passwd (Def: stanza)
    --debug <level>         prints debug messages
    --help                  prints this message
    --man                   prints man page (basically POD)

  RCM Options:
    --user <user>           database account. (Def: preset)
    --password <password>   database password (Def: preset)
    -S | --server <host>    rcm database server (Def: preset)
    -C | --config <file>    reads rcm options from that file

  find more information and more options with 'perldoc clone_sh_services.pl'
EOF
      ;

    exit 0;

}

__END__


# ------------------------------------------------------------------------- #
# begin POD
# ------------------------------------------------------------------------- #

=pod

=head1 NAME

Create host_allow.pl - Compare and Create /etc/host_allow file

=head1 SYNOPSIS

create host_allow

=head1 DESCRIPTION

This Script create host_allow
Check rcm avail
Create backup of actual host-allow
Make the host_allow

=head1 OPTIONS

=over 4



=item B<-?|--help>

Show a usage message

=item B<--man>

Show this manual.

=back

=head1 RCM Options

=over 4

=item B<-S|--server> I<database-server>

hostname of the machine, that runs the rcm database. Useful to connect other machines than the productive database.

=back

=head1 EXAMPLE


AUTHOR

S<Robert Rotter>

S<(C) Copyright Kyndryl>

=cut

# ------------------------------------------------------------------------- #
#  end POD
# ------------------------------------------------------------------------- #
