#!/usr/bin/perl -w
#use IO::All -utf8;
use strict;
use warnings;
use v5.10;
use Data::Dumper;
my $src = '/tmp/test_income';
#my $src1 = '/tmp/hosts_deny'


# NEW feed from /etc/inet.d
# AIX 1:root@dbkrmbm3:/root # cat inetd.con

#  1-for each new element which is not "ssh"
#  2-grep the new element from new host file > search in inet.d
#  3-feed them to the %hash


my %map = (
    "mq"           => "mqm",
    "godm"         => "godmd",
    "ssh"          => 'sshd',
    "MQSeries1414" => 'amqcrsta',
    "MQSeries1412" => 'amqcrsta'
);

my $re = join '|', map quotemeta, keys %map;
my $filename = $src;

my $data = read_file($filename);
$data =~ s/\b($re)\b/$map{$1}/gm;
write_file( $filename, $data );
sub read_file {
    my ($filename) = @_;

    open my $in, '<:encoding(UTF-8)', $filename
      or die "Could not open '$filename' for reading $!";
    local $/ = undef;
    my $all = <$in>;
    close $in;
    return $all;
}
sub write_file {
    my ( $filename, $content ) = @_;

    open my $out, '>:encoding(UTF-8)', $filename
      or die "Could not open '$filename' for writing $!";
    print $out $content;
    close $out;
    return;
}

exit;
