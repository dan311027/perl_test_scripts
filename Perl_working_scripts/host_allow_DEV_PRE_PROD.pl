#!/usr/bin/perl -w
#
# ------------------------------------------------------------------------- #
#
#   RCM - Reliable Configuration Management
#
#   (C) Copyright Kyndryl 2022
#   All Rights Reserved.
#
# ------------------------------------------------------------------------- #
#   Version:    DEV 
#
#   Module:     hosts_allow.pl
#
#   Author:     Robert Rotter
#
#   Date:       10.06.2022
#
#   Purpose:
#       grab all records related to hosts_allow file
#                 " ALL:    localhost"
#                 " sshd:   1234.rze.de.db.com"
#       create backup of /etc/hosts.allow
#       replace the with new version
#
#   Assumptions:
#     1  we always expect that $service, $function does never contain a ':'
#     2  working directory is /tmp
#     3  script directory and the diff evidence file are in:
#        "/var/applications/Hosts_allow/"
#     4 artifact files created in /tmp will be destroyed after script finish the job
#
#   Tested on SUSE    11.4  dbkdlin02
#             REDHAT  7.9   dbkdprov01
#             AIX     7.2   dbkdver01
#             Solaris 10    dbkdsol11
#---------------------------------------------------------------------------#      
#
#sub version 0.1 - defined more Perl like workflow - by xtrnrps
#sub version 0.2 - added RCM features - by xtrnrps	

use strict;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use feature qw(say);
use Architecture;
use Machine;
use Query;
use Record;
use Data::Dumper;
use File::Compare qw(compare);
use ConfigFile;
use Text::Diff;
use File::Copy;
use POSIX qw(strftime);
use RCM::MakeScript;



#------Delete unwanted file from /tmp if is still there ------#
my $src1          = "/tmp/hosts.allow_income_tmp";
my $cp_orig_tmp   = '/tmp/hosts.allow_tmp';
unlink $src1;
unlink $cp_orig_tmp;

if ( -e $src1 ) {
    print "File still exists!\n";
   }
if ( -e $cp_orig_tmp ) {
    print "File still exists!\n";
   }
else {
    print "File gone.\n";
   }

#------Retrieve with Querry the new hosts.allw file and modify ssh to sshd  add banner in file structure---#
#$src1 is the income file stored as host.allow_income

#---TEST FOR RCM Integration------

my $rcm = RCM::MakeScript->new({facility => 'application'}) or
die "MakeScript init failed: $@\n";

if (! RCM::Script::GetOptions() ) {
$rcm->Log->error ("Error in options/arguments");
usage(); # usage sub has to be defined somewhere!
exit 2;
}

#-----
open( IN, ">$src1" );
print IN"
#############################################
#
# /etc/hosts.allow  -  TCP Wrapper config file
#
# Created automatically hosts.allow.pl
#
# Do not modify: Changes will be lost !!!
#
ALL:    localhost\n";

while (<IN>) { print  $_; }

my $OS      = Architecture->OSName;
my $hostid  = Machine->hostid;
my $key     = 'tcpd_perm_by_hostid';
my $version = '1.0';
my $query   = Query->new( $key, $version );
my $results = $query->query( { hostid => $hostid } );

open( IN, ">>", "$src1" ) || die("error: $!");
foreach ( $results->records ) {
    my $rec = Record->new($_);
    {
        print IN $rec->getval('service') . ":\t"
          . $rec->getval('client') . "\n";
    }
}

print "\nGreat ! New Source File Created\n";
close(IN);

#-----Replace ssh with sshd----#

my $filename = $src1;

my $data = read_file($filename);
$data =~ s/ssh:/sshd:/gm;

write_file( $filename, $data );

sub read_file {
    my ($filename) = @_;

    open my $in, '<:encoding(UTF-8)', $filename
      or die "Could not open '$filename' for reading $!";
    local $/ = undef;
    my $all = <$in>;
    close $in;
    return $all;
}

sub write_file {
    my ( $filename, $content ) = @_;

    open my $out, '>:encoding(UTF-8)', $filename
      or die "Could not open '$filename' for writing $!";
    print $out $content;
    close $out;
    return;
}
print "\nExcelent ! SSH replced as SSHD\n";

#-----------COPY ORIGINAL AND MAKE BACKUP with DATE---#
#original file is the test one $src2=etc_host
#backup file with date is $bkp
#backup created incremental based on current DATE

my $date_tag = strftime( "_%d-%m-%Y-%H-%M", localtime );
my $src2     = '/etc/hosts.allow';
my $bkp      = '/etc/hosts.allow';

if ( copy( "$src2", "$bkp$date_tag" ) || die("error: $!") ) 
{
    print "\nNo worries Backup file success based on current date\n";
}

#--------------Retrieve te Host_allow ----#
#Retrive the local copy of /etc/hosts_allow file at working Directory in /tmp

if (copy ( "$src2", "$cp_orig_tmp" ) || die("error: $!") )
{
print "\nEtc_Hosts copyed as hosts.allow_tmp to TMP file \n";
}

#----Compare the income file with original file

if ( compare( "$src1", "$cp_orig_tmp" ) == 0 ) {
  print "\nOoops ! They're equal , script will Quit\n";
  die;
}

#--------------DIFF-----------#
#CHECK DIFF AND WRITE TO DIFF-FILE
#diff is based on income file created src1 test_income and etc_host(test file is test_in_place des1)
#diff file is $diff >> diff_previous
#if files are equal script will die

my $diff = ' /var/applications/Hosts_allow/diff_previous.txt';
my $rc = system("diff -c  $src1 $cp_orig_tmp >> $diff");
{print "\nDiff file created /\n"}


#------REPLACE WORKING FILE WITH NEW/TMP FILE--------------#
#this steep wil move the temporary file to original place
#chmod to 444 after file copyed back to /etc

my $mode = 0444;
if ( copy( "$src1", "$src2" ) || die("error: $!") ) 
{
chmod( "$mode", "$src2" )
}
{
    print "\nAmazing ! Replacement of ETC.HOSTS.allow SUCCESS !!\n";
}

#---delete the temp working files -----#

unlink $src1;
unlink $cp_orig_tmp;

if ( -e $src1 ) { 
    print "File still exists!\n";
   }   
if ( -e $cp_orig_tmp ) { 
    print "File still exists!\n";
   }   
else {
    print "File gone.\n";
   }   

exit;

############
# ------------------------------------------------------------------------- #
#  SUB usage
# ------------------------------------------------------------------------- #
sub usage {
    my $message = shift;

    print STDERR $0 . ": error: " . $message . "\n" if ( defined($message) );

    print STDERR <<EOF
  Usage: $0 [RCM Options] [--id Service-IDs] [more Options]

  Options:
    --id <service-id>       clone the listed service-ids (E.g. s1,s2,s3)
    --mode                  [insert|select] (Def: select)
    --unique                remove duplicate data
    --format <format>       output format: stanza, passwd (Def: stanza)
    --debug <level>         prints debug messages
    --help                  prints this message
    --man                   prints man page (basically POD)

  RCM Options:
    --user <user>           database account. (Def: preset)
    --password <password>   database password (Def: preset)
    -S | --server <host>    rcm database server (Def: preset)
    -C | --config <file>    reads rcm options from that file

  find more information and more options with 'perldoc clone_sh_services.pl'
EOF
      ;

    exit 0;

}

__END__


# ------------------------------------------------------------------------- #
# begin POD
# ------------------------------------------------------------------------- #

=pod

=head1 NAME

Create host_allow.pl - Compare and Create /etc/host_allow file

=head1 SYNOPSIS

create host_allow

=head1 DESCRIPTION

This Script create host_allow
Check rcm avail
Create backup of actual host-allow
Make the host_allow

=head1 OPTIONS

=over 4



=item B<-?|--help>

Show a usage message

=item B<--man>

Show this manual.

=back

=head1 RCM Options

=over 4

=item B<-S|--server> I<database-server>

hostname of the machine, that runs the rcm database. Useful to connect other machines than the productive database.

=back

=head1 EXAMPLE


AUTHOR

S<Robert Rotter>

S<(C) Copyright Kyndryl>

=cut

# ------------------------------------------------------------------------- #
#  end POD
# ------------------------------------------------------------------------- #

