#!/usr/bin/perl -w
#
# tested on SUSE   11.4 dbkdlin02
#           REDHAT 7.9  dbkdprov01
# ------------------------------------------------------------------------- #
#
#   RCM - Reliable Configuration Management
#
#   (C) Copyright Kyndryl 2022
#   All Rights Reserved.
#
# ------------------------------------------------------------------------- #
#   Version:    DEV
#
#   Module:     hosts_allow.pl
#
#   Author:     Robert Rotter
#
#   Date:       23.06.2022
#
#   Purpose:
#       grab all records related to hosts_allow file
#                  ALL:    localhost
#                  sshd:   1234.rze.de.db.com
#
#   Assumptions:
#       we always expect that $service, $function does never contain a ':'
#
# ------------------------------------------------------------------------- #
#sub version 0.1 - defined more Perl like workflow by xtrnrpx
#sub version 0.2 - check tmp file system

use strict;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use feature qw(say);
use Architecture;
use Machine;
use Query;
use Record;
use Data::Dumper;
use File::Compare qw(compare);
use ConfigFile;
use Text::Diff;
use File::Copy;
use POSIX qw(strftime);

#------Retrieve with Querry the new hosts.allw file and modify ssh to sshd  add banner in file structure---#
#$src1 is the income file stored as host.allow_income
my $src1 = '/tmp/host.allow_income';

open( IN, ">$src1" );
print IN"
#############################################
#
# /etc/hosts.allow  -  TCP Wrapper config file
#
# Created automatically host.allow.pl
#
# Do not modify: Changes will be lost !!!
#
ALL:    localhost\n";

while (<IN>) { print IN $_; }

my $hostid  = Machine->hostid;
my $key     = 'tcpd_perm_by_hostid';
my $version = '1.0';
my $query   = Query->new( $key, $version );
my $results = $query->query( { hostid => $hostid } );



my $filename = $src1;
my @list = ( 'ssh', 'shit', 'mq', 'rsync', 'smtp', 'MQSeries1414' );


my %transformations = (
    ssh          => 'sshd',
    mq           => 'mqm',
    MQSeries1414 => 'amqcrsta',

);
foreach my $item (@list) {
    if ( exists $transformations{$item} ) {
        $item = $transformations{$item};
    }
}

print $item;

open( IN, ">>", "$src1" ) || die("error: $!");
foreach ( $results->records ) {
    my $rec = Record->new($_);
    {
        print IN $rec->getval('service') . ":\t"
          . $rec->getval('client') . "\n";
    }
}




write_file( $filename, $data );

sub read_file {
    my ($filename) = @_;

    open my $in, '<:encoding(UTF-8)', $filename
      or die "Could not open '$filename' for reading $!";
    local $/ = undef;
    my $all = <$in>;
    close $in;
    return $all;
}

sub write_file {
    my ( $filename, $content ) = @_;

    open my $out, '>:encoding(UTF-8)', $filename
      or die "Could not open '$filename' for writing $!";
    print $out $content;
    close $out;
    return;
}
print "\nGreat ! New Source File Created\n";
close(IN);

exit;
