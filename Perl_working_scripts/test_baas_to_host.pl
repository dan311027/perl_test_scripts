use strict;
use IO::File;
use File::Spec::Functions qw(catfile);
use Getopt::Long;
use Pod::Usage;
use warnings;
#use lib qw( /usr/db/RCM/Classes );
#use RCM::Script;
#use Architecture;
#use Machine;
#use Query;
#use Record;


my $src = '/tmp/test_income';
my @list = ( 'ssh', 'mq', 'MQSeries1414' );

my %trans = (
    ssh          => 'sshd',
    mq           => 'mqm',
    MQSeries1414 => 'amqcrsta'
);

foreach my $item (@list) {
    {
        if ( exists $trans{$item} ) {
            $item = $trans{$item};
        }

        # first get propper node destination
        print "\n";
        open( IN, ">$src" );
        print IN"
#############################################
#
# /etc/hosts.allow  -  TCP Wrapper config file
#
# Created automatically hosts.allow.pl
#
# Do not modify: Changes will be lost !!!
#
ALL:    localhost\n";

        while (<IN>) { print $_; }

        #my $OS      = Architecture->OSName;
        my $hostid  = Machine->hostid;
        my $key     = "tcpd_perm_by_hostid";
        my $version = "1.0";
        my $query   = Query->new( $key, $version );
        my $results = $query->query( { hostid => $hostid } );

        open( IN, "+<", "$src" ) || die("error: $!");
        print Dumper( \$results );

        my $src = '';
        foreach ( $results->records ) {
            my $rec = Record->new($_);
            {
                print IN $rec->getval('service') . ":\t"
                  . $rec->getval('client') . "\n";
                $src = $rec->getval('src1');
            }#$hostid;
        }

        # adjust Nodename inside dsm.sys
        my $sysfile = catfile( $src, );

        my $IN = IO::File->new( $sysfile, 'r+' );

        # or die "Could not open the $sysfile!";
        my @data = <$IN>;
        $IN->close();

        my $IN2 = IO::File->new( $sysfile, 'w' );
        foreach (@data) {
            s/(service\W*).*($item)/$1$2/g;
            print $IN2 $_;
        }
        $IN2->close();
        print "\n";
    }
}
