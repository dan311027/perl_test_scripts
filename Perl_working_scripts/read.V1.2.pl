#check SSHD PID or quit
my @args = ('cat /var/run/sshd.pid');
system(@args) == 0
  or die "system @args failed: $?";
exit;

=cut
#-------------------------START--------------------------------------------------#
#DELETE UNVANTED/OLD FILE FROM WORKING DIRECTORY
#to be decided
my $file = "/home/xtrnrps/perl_test/test_income.txt";
unlink $file;

if ( -e $file ) {
    print "File still exists!\n";
}
else {
    print "File gone.\n";
}

##############################################################

#-----------COPY ORIGINAL file to working dir.AND MAKE BACKUP with DATE---#
#original file is the test one des1=etc_host(test file is test_in_place)
#backup created incremental based on current DATE

my $date_tag = strftime( "_%d-%m-%Y-%H-%M", localtime );
#my $des2 = '/etc/hosts.allow';
my $des1 = '/etc/hosts.allow';
my $dest_tmp_file = '/home/xtrnrps/perl_test/destination_tmp_file.txt';

if ( system("cat $des1 > $des1$date_tag") == 0 ) {
    print "\nBackup file success based on current date\n";
}

if ( system("cat $des1 >$dest_tmp_file") == 0 ) {
    print "\nOriginal file copyed to Destination_tmp\n";

###############################################################

#------QUERRY START AND CREATE COPY IN TMP  BASED On RCM ---#
#RETRIEVE THE HOSTS_FILE  AND WRITE TO TEMP FILE
#demo file is src1 = test_income.txt

my $src1 = '/home/xtrnrps/perl_test/test_income.txt';
#my $des1   = '/home/xtrnrps/perl_test/test_in_place.txt';
my $query  = Query->new( 'tcpd_perm_by_hostid', '1.0' );
my $result = $query->query( { hostid => 'dbkpbecm22.rze.de.db.com' } );

open( DATA, ">$src1" );

print DATA"
#############################################
#
# /etc/hosts.allow  -  TCP Wrapper config file
#
# Created automatically at $(date +'%Y-%m-%d %H:%M')
#
# Do not modify: Changes will be lost !!!
#
ALL:    localhost\n";

while (<DATA>) {
    print DATA $_;

}

foreach my $rec ( $result->records ) {
    my $rec        = Record->new($rec);
    my @attributes = $rec->getattrs();          # get list of attributes
    my $hostid     = $rec->getval('hostid');    # get value
    my $daemon     = $rec->getval('service');
    my $client     = $rec->getval('client');

    if ( open( my $FH, ">>", "$src1" ) || die("error: $!") ) {
        print $FH $rec->getval('service') . ":\t"
          . $rec->getval('client') . "\n";
        close($FH);
    }
}
print "\nNew Source File Created\n";

close(DATA);


##################################################################

#--------------DIFF-----------#
#CHECK DIFF AND WRITE TO DIFF-FILE
#diff is based on income file created src1 test_income and etc_host(test file is test_in_place des1)
#diff file is $diff - diff_previous
#if files are equal script will die
#my $des1 = '/home/xtrnrps/perl_test/test_in_place.txt';
my $diff = '/home/xtrnrps/perl_test/diff_previous.txt';
my $rc = system("diff -u  $src1 $dest_tmp_file >> $diff");
my $dest_tmp_file = '/home/xtrnrps/perl_test/destination_tmp_file.txt';

if ( compare( "$src1", "$dest_tmp_file" ) == 0 ) {
    print "\nThey're equal , script will Quit\n";
    die;
}


##################################################################

#------REPLACE WORKING FILE WITH NEW/TMP FILE--------------#
#this steep wil move the temporary file to original place(here is for test purpose $final_dest_file)

my $final_dest_file = '/home/xtrnrps/perl_test/final_dest_file.txt';

if ( system("cat $dest_tmp_file > $des1") == 0 ) {
    print "\nReplacement of ETC_HOSTS.allow SUCCESS !! \n";
}

exit;


