use warnings;
use strict;
use feature 'say';

my %hash = (
    ssh => 'sshd',
    key2 => 'value2',
    key3 => 'value3',
);


my $src = '/tmp/test_income';

open my $IN, '+<', $src;

my %r;
foreach (<>) {
    chomp;
    my ( $k, $v ) = split /,/sm;
    $r{$k} = $v;
    print $v;
}
my $re = '(' . join( '|', keys %r ) . ')';



while (<$src>) {
    s/$re/$r{$1}/g;
print $IN print;
}

