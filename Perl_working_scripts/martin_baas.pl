#!/usr/db/bin/perl

=pod

=head1 NAME

make_baas_node.pl

=head1 SYNOPSIS

make_baas_node.pl [OPTIONS]

=head1 OPTIONS

=over 4

=item -h | --hostid

Deploy all nodes defined in ADSM.BAAS_NODE table.

=item -n | --nodeid

Only specified nodes will be deployed (see RCM.BAAS_NODE) table for the reference.

=item -a | --all

Deploy all BaaS nodes assigned to current hostid (Machine->hostid is supplied). This option is designed mainly for server provisioning automation.

=item -d | --debug

This will dump out some datastructures used in the script. Useful mainly for development of new versions.

=back

=cut

use strict;
use IO::File;
use File::Spec::Functions qw(catfile);
use Getopt::Long;
use Pod::Usage;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use RCM::Script;
use Architecture;
use Machine;
use Query;
use Record;
use Data::Dumper;

my $debug = 0;
my $all   = 0;
my $hostid;
my @nodes;
my @nodeids;

my $TSMScript = '/applications/tsm/client/CURRENT/Scripts/make_TSMClient.pl';
my $TSMDir    = '/TSM/ctsm';

my $rcm = RCM::Script->new() or die "Could not set up an RCM client!";
$rcm->Log->info('Starting BaaS configuration deployment ... ');

my $dummy = RCM::Script::GetOptions(
    "d|debug!"    => \$debug,
    "a|all!"      => \$all,
    "h|hostid=s"  => \$hostid,
    "n|nodeid=s@" => \@nodes,
) or pod2usage( -verbose => 1 );

pod2usage( -verbose => 1 )
  unless defined $nodes[0]
  or defined $hostid
  or defined $all;

if ($all) {
    $hostid = Machine->hostid;
}

if ($hostid) {

    # first retrieve all BAAS nodes asociated to this host
    $rcm->Log->info("Retrieving BaaS configuration for HOSTID=$hostid ... ");
    my $key     = "baas_client_nodes_by_hostid";
    my $version = "1.0";
    my $query   = Query->new( $key, $version );
    my $results = $query->query( { hostid => $hostid } )
      or die "Could not retrieve data from RCM!";
    print Dumper( \$results ) if $debug;

    # Get all nodeids
    foreach ( $results->records ) {
        my $rec = Record->new($_);
        push @nodeids, $rec->getval('nodeid')
          or die "Could not process the found nodes!";
    }
    $rcm->Log->info( "Found following nodes: " . join( ' ', @nodeids ) );

}

# if -n was specified, check now if these node exists
# if so, add them to @nodeids array which is processed later on.
foreach my $node (@nodes) {
    $rcm->Log->info("Checking if BaaS node $node exists ... ");
    my $query  = Query->new( 'baas_client_nodes_by_nodeid', '1.0' );
    my $result = $query->query( { nodeid => $node } );
    my $count  = 0;
    foreach ( $result->records ) {
        my $rec = Record->new($_);
        unless ( $rec->getval('nodeid') ne $node ) {
            $rcm->Log->info("BaaS node $node found.");
            push @nodeids, $node;
            $count++;
        }
    }
    $rcm->Log->warning(
        "BaaS node $node is not registered in RCM, skipping it .... ")
      if $count == 0;
}

# at least one nodeid has to be defined!
if ( defined $nodeids[0] ) {
    foreach my $node (@nodeids) {

        # first get propper node destination
        print "\n";
        $rcm->Log->info("Retrieving details about node: $node ... ");
        my $key     = "baas_client_nodes_by_nodeid";
        my $version = "1.0";
        my $query   = Query->new( $key, $version );
        my $results = $query->query( { nodeid => $node } )
          or die "Could not retrieve data from RCM!";
        print Dumper( \$results ) if $debug;
        my $cfg_path = '';

        foreach ( $results->records ) {
            my $rec = Record->new($_);
            $cfg_path = $rec->getval('cfg_path');
        }

        $rcm->Log->info("Running $TSMScript -n $node ... ");
        qx($TSMScript . " -n " . $node);

        # adjust Nodename inside dsm.sys
        my $sysfile = catfile( $TSMDir, $cfg_path, $node, 'dsm.sys' );
        $rcm->Log->info("Adjusting configuration in $sysfile ... ");
        my $fh = IO::File->new( $sysfile, 'r+' )
          or die "Could not open the $sysfile!";
        my @data = <$fh>;
        $fh->close();

        my $fh2 = IO::File->new( $sysfile, 'w' );
        foreach (@data) {
            s/(NODename\W*).*($node)/$1$2/g;
            print $fh2 $_;
        }
        $fh2->close();
        print "\n";
    }
}
else {
    $rcm->Log->error("No valid NODEID was supplied, exiting now ... ");
    exit 2;
}
$rcm->Log->info('Done.');

=pod

=head1 AUTHOR

Martin Sukany (martin.sukany@kyndryl.com)

=cut
