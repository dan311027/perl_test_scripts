#!/usr/bin/perl -w

use warnings;
use strict;
use v5.10;
use common::sense;
use Data::Dumper qw(Dumper);
use File::Slurp;
use Tie::File;

#my $src  = "/tmp/etc.inet.source";
#my $src1 = "/tmp/etc.inet.des";

my $ifile = '/tmp/etc.inet.source';
my $ofile = '/tmp/etc.inet.dest';

open( my $ifh, '+<',  $ifile ) or die $!;
open( my $ofh, '>>', $ofile ) or die $!;

=pod
###############
#can be used to remove just # lines
###############
while (<$ifh>) {
    print $ofh $_ unless /^##?/;
}
=cut
#=pod
sub main {

    while ( my $String = <$ifh> ) {
        if ( $String =~ /\b -m \b/ )

          #  if (_$String =~ /\bsteam\b/)

        {
            say "$String";
            print $ofh "$String"; 
        }
    }
}
#open( my $ofh, '+>>', $ofile ) or die $!;
my $String = read_file($ofile);
my $str    = "$String";
my @fields = split / /, $str;

#my ( $read, $serv, $sock, $prot, $mode, $user, $path, $prog, $rest ) = split / /,$str;

my ( $read, $path ) = ( split ' ', $str )[ 0, 6 ];
print "$read\t";
print "$path\n";
print $ofh "$read\t";
print $ofh "$path\n";
main();
close($ifh);
close($ofh);

=cut
=pod
#sub main 
#{
#$|++;
#$/ = ' ';
open( my $ofh, '+>', $ofile ) or die $!;
my $String = read_file($ofile);
my $str    = "$String";
my @fields = split / /, $str;
#my ( $read, $serv, $sock, $prot, $mode, $user, $path, $prog, $rest ) = split / /,$str;

my ( $read, $path ) = ( split ' ', $str )[ 0, 6 ];
print "$read\t";
print "$path\n";
print $ofh "$read\t";
print $ofh "$path\n";
#}
#main();

=pod
sub main {
#my $str =  "root:*:0:0:System Administrator:/var/root:/bin/sh";
my $str = "MQSeries1414:stream:tcp:nowait:mqm:/etc/tcpd:/usr/mqm/bin/amqcrsta:-m:QM.A104091";
#my ( $username, $password, $uid, $gid, $real_name, $home, $shell ) = split /:/,
my ($read, $serv, $sock, $prot, $mode, $user, $path, $prog, $rest)  = split /:/,$str;
print "$read\n"; say "$path\n";
print ;
=cut



