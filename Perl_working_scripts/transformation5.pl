use v5.10;
use strict;
use warnings;
use Data::Dumper;

#my $cp_orig_tmp = '/host.allow_income';
my $src1 = '/tmp/test_income';
my @list = ( 'ssh:', 'mq:', 'MQSeries1414:' );

my %transformations = (
    'ssh:'          => 'sshd:',
    mq              => 'mqm:',
    'MQSeries1414:' => 'amqcrsta:'

);

#open( IN, `<`, $src1 ) or die $!;
open( IN, '>>', $src1 ) or die $!;
foreach my $item (@list) {
    if ( exists $transformations{$item} ) {
        $item = $transformations{$item};
    }

    #say "$item";

    say IN "$item\n";
    close(IN);
}
