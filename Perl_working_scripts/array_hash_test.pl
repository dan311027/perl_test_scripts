=pod
my %hash = (
    'MyVehicle' => 'Car',
    'Model'     => 1234,
    'Speed'     => 60.7,

    # value type hash
    'Traffic' => {
        'Red'    => 'Stop',
        'Yellow' => 'Look and move',
        'Green'  => 'Go'
    },

    # value type array
    'AllVehicles' => [ 'Car', 'Cycle', 'Bus', 'Auto' ]
);

# Loop over the key storing array
my @array = @{ $hash{'AllVehicles'} };
print "AllVehicles include \n";

# for loop to loop over the array
foreach my $ele (@array) {
    print "$ele\n";
}

# Loop over the key storing hash
print "\nTraffic includes\n";

# for loop to loop over the hash
foreach my $val ( keys %{ $hash{'Traffic'} } ) {
    print "Key : $val, value : $hash{'Traffic'}{$val}\n";
}
=cut
use strict;
use warnings;

my $src1 = '/tmp/host.allow_income';    
my @list = ('ssh:','shit','mq','rsync','smtp','MQSeries1414'); 

my %transformations = ( 
  ssh => 'sshd',
  mq => 'mqm',
  MQSeries1414 => 'amqcrsta'

);

open( FILE, '<', $src1 );
my @lines = <FILE>;

#close(FILE);

foreach my $item (@list) {
    if ( exists $transformations{$item} ) {
        $item = $transformations{$item};
    }
}
open( FILE, '>', $src1 );
print FILE @lines;
close(FILE);
