#!/usr/bin/perl -w

use warnings;
use strict;
use v5.10;
use Data::Dumper;

my $src  = '/tmp/test_income';

my @replacer = ( "godm", "mq:", "ssh:", "MQSeries1414:", );
my @tobereplaced = (
    'mq:'           => 'mqm:',
    "godm"          => "godmd:",
    "ssh:"          => 'sshd:',
    "MQSeries1414:" => 'amqcrsta:'
);

open my $IN, '+<', $src;
@tobereplaced = map split, @tobereplaced;
@replacer     = map split, @replacer;

my %replacements;
@replacements{@tobereplaced} = @replacer;

my $pat = join '|', map quotemeta, @tobereplaced;
my $re  = qr/$pat/;

while (<$IN>) {
    s/\b($re)\b/$replacements{$1}/g;
    print;
    say $IN $re;
}
