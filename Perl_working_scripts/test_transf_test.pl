#!/usr/bin/perl -w

use strict;
use warnings;

my $src  = '/tmp/test_income';
my @list = ( 'ssh', 'mq', 'MQSeries1414', );
#$list = join( "|", map { quotemeta } keys %transf );

my %transf = (
    mq           => 'mqm:',
    ssh          => 'sshd:',
    MQSeries1414 => 'amqcrsta:'
);
open( my $IN, '+<', $src );

foreach my $item (@list) {

    if ( exists $transf{$item} ) { $item = $transf{$item}; }
    
    say $IN $item;
}

close $IN;
