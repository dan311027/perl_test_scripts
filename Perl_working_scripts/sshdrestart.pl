#!/bin/perl -w
use strict;
use warnings;

my @args = ('cat /var/run/sshd.pid');
my @args1 = ('systemctl restart sshd');
my @args2 = ('systemctl restart rsyslog');
=pod
system(@args1) == 0
  or die "system @args1 failed: $?";
   {
    print "\nSSHD Daemon Restarted\n";
   }
=cut
system(@args) == 0
  or die "system @args failed: $?";
    {
         print "\nNice SSH PID found moving forward\n";
    }
  
system(@args2) == 0
  or die "system @args2 failed: $?";
    {
        print "\nRSYSLOG Daemon Restarted\n";
    }
