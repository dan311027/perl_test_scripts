#! /usr/db/bin/perl -w
#! /usr/db/bin/perl -w
# ------------------------------------------------------------------------- #
use strict;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use Architecture;
use Machine;
use Query;
use Record;
use Data::Dumper;

#---------------------------------------------------------------------------#

my $q      = Query->new( 'tcpd_perm_by_hostid', '1.0' );
my $result = $q->query( { hostid => 'dbkpbecm22.rze.de.db.com' } );

# ------------------------------------------------------------------------- #
foreach my $rec ( $result->records )
{
    my $rec        = Record->new($rec);
    my @attributes = $rec->getattrs();          # get list of attributes
    my $hostid     = $rec->getval('hostid');    # get value
    my $daemon     = $rec->getval('service');
    my $client     = $rec->getval('client');
    print $rec->getval('service') . ":\t" . $rec->getval('client') . "\n";
}

#####
#from RCM querries
#! /usr/db/bin/perl
use strict;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use Architecture;
use Query;
use Record;

my $q      = Query->new( 'ip_if_by_hostid', '3.0' );
my $result = $q->query( { hostid => 'ibmprcm01.ffm.de.sni.ibm.com' } );

# loop over records in result
foreach my $rec ( $result->records ) {

    # let Record.pm split the record into fields
    my $rec = Record->new($rec);

    my @attributes = $rec->getattrs();            # get list of attributes
    my $hostname   = $rec->getval('hostname');    # get value
    my $ip_address = $rec->ip_address();
    \                                             # alternative to get value

      print "\* got ip_address:$ip_address, hostname:$hostname\n";
}
###########################################

#LATEST VERSION 
#! /usr/db/bin/perl -w
# ------------------------------------------------------------------------- #
use strict;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use Architecture;
use Machine;
use Query;
use Record;
use Data::Dumper;

#---------------------------------------------------------------------------#
my $src1   = '/tmp/read_file.txt';
my $q      = Query->new( 'tcpd_perm_by_hostid', '1.0' );
my $result = $q->query( { hostid => 'dbkpbecm22.rze.de.db.com' } );

# ------------------------------------------------------------------------- #
foreach my $rec ( $result->records ) {
    my $rec        = Record->new($rec);
    my @attributes = $rec->getattrs();          # get list of attributes
    my $hostid     = $rec->getval('hostid');    # get value
    my $daemon     = $rec->getval('service');
    my $client     = $rec->getval('client');

    #print $rec->getval('service') . ":\t" . $rec->getval('client') . "\n";
    open OUTPUT, '>>', $src1 or die "Can't create filehandle: $!";
    select OUTPUT;
    print OUTPUT $rec->getval('service') . ":\t" . $rec->getval('client') . "\n";
    close(OUTPUT);

}
exit;
