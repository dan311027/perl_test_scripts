#Perl read/write file in scalar context
#file handler need (FH) to read the file need to $line = <FH>;
#or use while
#while (<FH>) {
#
#    #...
#}
#test open file

#use Data::Dumper qw(Dumper);
use strict;
use warnings;

#open file
my $filename = "/tmp/read_file.txt";
open(FH, "<", "$filename") or die $!;
print("FIle $filename opend success!\n");

close(FH);

#read file

#my $filename = '/tmp/read_file.txt';    #### already defined

open( FH, '<', $filename ) or die $!;  ##using the context as read the file

while (<FH>) {                         #usig the while to read the fil
    print $_;                          #print the file
}

close(FH);                             #close the open and the read           

#######################  NOT RELEVANT
#       Append to file       #

my $src1 = '/tmp/add.text.to.read.txt';    #text to be added to read,txt
# open destination file for writing
open( DES, '>>', $des ) or die $!;


#open and read backup hosts.allow file

open( FH, '<', $des ) or die $!;    #using the context as read the file

while (<FH>) {    #usig the while to read the file
    print $_;     #print the file
}

close(FH);        #close the open and the read

print "3  Backup readed!\n";

