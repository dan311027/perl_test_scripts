#!/usr/bin/env perl
#   @(#) $Id: clone_sh_services.pl,v 1.41 2020/08/06 09:22:26 Andreas_Jung1 Exp $
# ------------------------------------------------------------------------- #
#
#   RCM - Reliable Configuration Management
#
#   (C) Copyright IBM Corporation 1999,2004,2011
#   All Rights Reserved.
#
# ------------------------------------------------------------------------- #
#
#   Module:     clone_sh_services.pl
#
#   Author:     Dr. Frank-Christian Otto, Andreas Jung
#
#   Date:       28.08.2000
#
#   Purpose:    
#       grab all records related to a shared service and print them to stdout.
#
#   Assumptions: 
#       we always expect that $service, $function does never contain a ':'
#
# ------------------------------------------------------------------------- #


use strict;
use warnings;

use File::Basename qw(dirname);
use File::Spec::Functions qw(catdir catfile);
use File::Temp qw/ tempfile tempdir /;
use FindBin qw($Bin);
use Try::Tiny;
use Getopt::Long;                             # I love those GNU-like long options
Getopt::Long::Configure("no_ignore_case");    # now -q and -Q are different!

use Cwd;
my $currdir = getcwd;

# to find Susi libs
use lib ( catfile( dirname($0), '..', 'lib' ));

use Susi::CommonTools qw(get_password susilog manual error);
use Susi::Client;
use Susi::ClientQuery;
use Susi::Defaults;

binmode STDOUT, ":encoding(UTF-8)";

# RCM parameter
my $rcm_host;       # RCM server
my $rcm_port;       # susiserver port (default 8421)
my $rcm_user;       # database login user
my $rcm_inst;       # SID of the database to connect to
my $rcm_pwd;        # password for $rcm_user

my $help;
my $man;
my $config_file;
my $output_file;
my $format  = 'passwd';
my $debug   = 0;
my $mode    = 'select'; # defaukt is just read and output
my $unique  = 1;     # remove duplicate data by default
my $id;
my $target;

my @Lids;

my $cleanup = 1;
my @service_function;
my $sql_file = "$currdir/services.sql";

my $dummy = GetOptions(
    "mode=s"                             => \$mode,
    "id=s"                               => \$id,
    "unique!"                            => \$unique,
    "target=s"                           => \$target,
    #
    "help|?"                             => \$help,
    "man"                                => \$man,
    "C|config-file=s"                    => \$config_file,
    "f|output-file|outfile|file=s"       => \$output_file,
    "F|output-format|outformat|format=s" => \$format,
    "d|debug:+"                          => \$debug,

    # RCM Options:
    "u|user=s"     => \$rcm_user,
    "p|password=s" => \$rcm_pwd,
    "S|server=s"   => \$rcm_host,
    "P|port=s"     => \$rcm_port,
    "I|instance=s" => \$rcm_inst,
);

usage("wrong options\n") if ( !$dummy );
usage()                  if ($help);
manual()                 if ($man);

# defaults for database access
my $defaults = {
    user        => $rcm_user,
    password    => $rcm_pwd,
    server      => $rcm_host,
    format      => 'stanza',
    inst        => 'rcm',
    port        => 8422,
};

if ($rcm_host and $rcm_user and $rcm_pwd) {
    # skip reading defaults file
} else {
    my %DefParams;
    $DefParams{'dontask4pwd'} = 1 if ( $rcm_pwd || $rcm_user );
    $DefParams{'config_file'} = $config_file if ($config_file);
    
    $defaults = Susi::Defaults::init( \%DefParams );
} 

$rcm_port = $defaults->{'port'}   unless ($rcm_port);
$rcm_host = $defaults->{'server'} unless ($rcm_host);
$rcm_inst = $defaults->{'inst'}   unless ($rcm_inst);

if ($rcm_user) {
    unless ($rcm_pwd) {
        if ( $rcm_user eq $defaults->{'user'} and $defaults->{'password'} ) {
            $rcm_pwd = $defaults->{'password'};
        } else {
            $rcm_pwd = get_password($rcm_user);
        }
    }
} elsif ( $defaults->{'user'} ) {
    $rcm_user = $defaults->{'user'};
    $rcm_pwd = $defaults->{'password'} unless ($rcm_pwd);
} else {

    # very unlikely case
    print STDERR "username for RCM query:";
    $rcm_user = <STDIN>;
    chomp $rcm_user;
    $rcm_pwd = get_password($rcm_user);
}

#
# if no in "select" mode we need  a real login to produce correct results
#
if ( $mode ne "select" and $rcm_user eq "rcmview" ) {
    usage( "not in \"select\" mode.\n -> restart with your personal database login\n" );
}

unless ($mode =~ /select|insert/) {
    usage("mode $mode is not supported\n");
}

if ($mode eq 'insert' and not defined $target) {
    usage("you have to supply a target server for the data insert\n");
}

print STDERR " * connecting to database $rcm_user",
  ( $debug > 2 ) ? "/$rcm_pwd" : "", "\@$rcm_host", ( $debug > 1 ) ? " Inst:$rcm_inst Port:$rcm_port" : "", "\n"
  if ($debug);

# connect to database
my $susi = Susi::Client->new( { host     => $rcm_host,
                                instance => $rcm_inst,
                                port     => $rcm_port,
                                user     => $rcm_user,
                                password => $rcm_pwd
                              }
);

# create object / perform $susi->login implicitely
my $client = Susi::ClientQuery->new( $susi,
                                     {  mode       => 'select',
                                        table_sort => 0,
                                        debug      => $debug
                                     }
);

# set record and field separator:
my $rsep = $susi->RSEP;
my $fsep = $susi->FSEP;

# check whether format is valid
$client->validate_print_format($format);

if ($id) {
    push( @Lids, split (/[,\s*]/, $id) );
} else {
    print STDERR "### ERROR: you have to tell me the id(s) to clone\n";
    exit 1;
}

# TEST
print STDERR " * ID-LIST: ", join( ", ", @Lids ), "\n" if ($debug);

#
# generate list of service:function pairs
#
 my $query;
for my $sid (@Lids) {
    $query =  "select service||':'||function from rcm.shared_services_vw "
            . " where service_id = '$sid'";
    my ($status, $msg) = $susi->query($query);
    die("Error reading from RCM -- $msg\nLast action -- $query\n")
        if ($status != 1403 && $status > 0);
    push @service_function, (split /$rsep/, $msg);
    if ($debug >=4) {
        print STDERR "* list of shared service/function pairs:\n";
        foreach (@service_function) {
            print STDERR "\t$_\n";
        }
    }
}

#
# retrieve SERVICE_PLUS data + SERVICE_VGLVFS + SERVICE_CRON + SUDO_PERM
#
foreach my $svc_id (@Lids) {
    print STDERR " * read info of service_id $svc_id\n" if ($debug);
    $client->collect_table_data( "rcm.service_plus", 
        "select * from rcm.service_plus where service_id = '$svc_id'" );
    $client->collect_table_data( "rcm.service_cron", 
        "select * from rcm.service_cron where service_id = '$svc_id'" );
    $client->collect_table_data( "rcm.service_vglvfs",
        "select * from rcm.service_vglvfs where service_id = '$svc_id'" );

    # SUDO_PERM+SUDO_CMND_ALIAS
    $client->collect_table_data( "rcm.sudo_perm", 
        "select * from rcm.sudo_perm where service_id = '$svc_id'" );
    $client->collect_table_data( "rcm.sudo_incl_perm", 
        "select * from rcm.sudo_incl_perm where service_id = '$svc_id'" );
    $client->collect_table_data( "rcm.sudo_cmnd_alias",
        "select * from rcm.sudo_cmnd_alias where cmnd_alias in (select cmnd_alias from rcm.sudo_perm where service_id = '$svc_id')"
    );

    # SERVICE_INETD
    $client->collect_table_data( "rcm.service_inetd", 
        "select * from rcm.service_inetd where service_id = '$svc_id'" );

    # add dependent INETD data here?

    # SERVICE_TCPD
    $client->collect_table_data( "rcm.service_tcpd", 
        "select * from rcm.service_tcpd where service_id = '$svc_id'" );

    # add dependent TCPD data here?

    # SERVICE_BUNDLES
    $client->collect_table_data( "rcm.service_bundles",
        "select a.* from rcm.service_bundles a, rcm.services b where b.service_id = '$svc_id' and a.bundle_name = b.bundle and a.os = b.os"
    );

    # Solaris Patches coded in SERVICE_BUNDLES
    $client->collect_table_data( "rcm.service_bundles",
        "select a.* from rcm.service_bundles a, rcm.services b where b.service_id = '$svc_id' and a.bundle_name = b.bundle and a.os = b.os||'_Patch'"
    );
}

#
# work on service_function list
#
foreach (@service_function) {
    my ( $svc, $fcnt ) = ( split ':', $_ );

    if ( $debug >= 3 ) {
        print STDERR " * working on service=\"$svc\" function=\"$fcnt\"\n";
    }

    # APPLICATIONS
    $client->collect_table_data( "rcm.applications",
        "select * from rcm.applications where service = '$svc' and function = '$fcnt'" );

    # SERVICE_ACCOUNTS
    $client->collect_table_data( "rcm.service_account",
        "select * from rcm.service_account where service = '$svc' and function = '$fcnt'" );

    # SERVICE_ORDER
    $client->collect_table_data( "rcm.service_order",
        "select * from rcm.service_order where service = '$svc' and function = '$fcnt'" );

    # SERVICE_PREREQ
    $client->collect_table_data( "rcm.service_prereq",
        "select * from rcm.service_prereq where service = '$svc' and function = '$fcnt'" );
    $client->collect_table_data( "rcm.service_prereq",
        "select * from rcm.service_prereq where req_service = '$svc' and req_function = '$fcnt'" );

    # SERVICE_CONTAINER
    $client->collect_table_data( "rcm.service_container",
        "select * from rcm.service_container where service = '$svc' and function = '$fcnt'" );

    # APPL_TEMPLATE
    $client->collect_table_data( "rcm.appl_template_info",
        "select service, function, filename, description from rcm.appl_template where service = '$svc' and function = '$fcnt'"
    );

}

#
# retrieve data only dependent on service name
#
foreach my $svc ( $client->list_one_column( "rcm.service_plus", "service" ) ) {
    # service domain hierarchies are installation specific
    # SERVICE_DOMAIN
    #$client->collect_table_data ("service_domain",
    #				 "select * from rcm.service_domain where service = '$svc'" );

    # service domain server lists are installation specific, too
}

#
# retrieve storage data
#
foreach my $vglvfs ( $client->list_one_column( "rcm.service_vglvfs", "vglvfs_id" ) ) {
    $client->collect_table_data( "rcm.volume_groups", 
        "select * from rcm.volume_groups where vglvfs_id = '$vglvfs'" );
    $client->collect_table_data( "rcm.vg_disks",      
        "select * from rcm.vg_disks where vglvfs_id = '$vglvfs'" );
    $client->collect_table_data( "rcm.fs",
        "select * from rcm.fs where vglvfs_id = '$vglvfs'" );
    $client->collect_table_data( "rcm.logical_volumes",
        "select * from rcm.logical_volumes where vglvfs_id = '$vglvfs'" );
}

#
# retrieve cron data
#
foreach my $cronid ( $client->list_one_column( "rcm.service_cron", "cronid" ) ) {
    $client->collect_table_data( "rcm.cron_def", 
        "select * from rcm.cron_def where cronid = '$cronid'" );
}

# sort data if not in "select" mode
#   not sure why this was necessary, ajung 07.06.2018
#if ( $mode ne "select" ) {
#    if ( $debug >= 2 ) {
#        print STDERR " * performing column sort due to mode=\"$mode\"\n";
#    }
#    $client->sort_columns();
#}

# remove duplicate records if requested
if ($unique) {
    print STDERR " * removing duplicate data\n"
        if ($debug);
    $client->remove_duplicate_data();
}

# print results to file or stdout
my @buffer = $client->print_table_data($format);

if ($mode eq "select") {
    if ( defined($output_file) ) {
        open( OUTPUT, ">" . glob($output_file) )
            or die "can't open file $output_file for writing: $!\n";
        foreach (@buffer) {
            print OUTPUT "$_\n";
        }
        close(OUTPUT);
    } else {    # write to stdout
        foreach (@buffer) {
            print "$_\n";
        }
    }
} elsif ($mode eq "insert") {
    my ($fh, $filename) = tempfile();
    foreach (@buffer) {
#        print "$_\n" if ($mode eq 'preview');
        print $fh "$_\n";
    }
    my $insert_cmd = catfile( $Bin, 'rcm_insert.pl');
    $insert_cmd .= " -S $target";
    $insert_cmd .= " -user $rcm_user";
    $insert_cmd .= " -pass $rcm_pwd";
    $insert_cmd .= " -file $filename";
    $insert_cmd .= " -format passwd";
    $insert_cmd .= " -execmode internal";    
    $insert_cmd .= " -debug $debug";
    
    print " * $insert_cmd\n" if ($debug > 1);   
    print " * executing rcm_insert.pl on $target\n" if ($debug);
    system($insert_cmd);

    unlink ($filename) if ($cleanup);    
} else {
    print "mode $mode not supported\n";
    exit 1;
}


# ------------------------------------------------------------------------- #
#  SUB usage
# ------------------------------------------------------------------------- #
sub usage {
    my $message = shift;

    print STDERR $0 . ": error: " . $message . "\n" if ( defined($message) );

    print STDERR <<EOF
  Usage: $0 [RCM Options] [--id Service-IDs] [more Options]

  Options:
    --id <service-id>       clone the listed service-ids (E.g. s1,s2,s3)
    --mode                  [insert|select] (Def: select)
    --unique                remove duplicate data
    --format <format>       output format: stanza, passwd (Def: stanza)
    --debug <level>         prints debug messages
    --help                  prints this message
    --man                   prints man page (basically POD)
    
  RCM Options:
    --user <user>           database account. (Def: preset)
    --password <password>   database password (Def: preset)
    -S | --server <host>    rcm database server (Def: preset)
    -C | --config <file>    reads rcm options from that file

  find more information and more options with 'perldoc clone_sh_services.pl'
EOF
      ;

    exit 0;

}

__END__


# ------------------------------------------------------------------------- #
# begin POD 
# ------------------------------------------------------------------------- #

=pod

=head1 NAME

clone_sh_services.pl - clone shared services

=head1 SYNOPSIS

clone_sh_services.pl [--id I<service_id1,service_id2, .. service_idn>]

=head1 DESCRIPTION

This script reads information about shared services and prepares them for insert into another database instance. It will print the output in passwd format to stdout. To process the output of the clone_sh_service.pl with rcm_insert.pl the --format passwd option has to be set.

Shared services are defined as services having the shared flag set. This can be checked or set in the Options menue in SERVICE_PLUS.

clone_sh_service.pl is the script that handles the generic synchronisation of services between two rcm instances. With the option B<--id> it is able to transfer the data of one single service_id. 

=head1 OPTIONS

=over 4

=item B<--id> I<service-id>

Clone only this service id. multiple service ids are supported "-id 1234,2435,3652"

=item B<--mode> I<mode>

the value of the mode parameter tells the script what to do. With mode=select the script just reads the service configuration data and prints it to stdout. 
With mode=insert the script will feed the data retrieved into rcm_insert.pl. In this case you'd need the define the target rcm host.
Default is select.

=item B<--target> I<target RCM Server>

This parameter is necessary in --mode=select to define where to insert the selected data.

=item B<--[no]unique>

When retrieving data for more then an service_id you might end up with duplicates in some related data.

--nounique would not removes duplicates.

default: --unique

=item B<-?|--help>

Show a usage message

=item B<--man>

Show this manual.

=item B<-d|--debug> I<debug_level>

If <debug_level> is set to a value greater than zero you will see
what is going on. The higher the value the noisier rcm_insert.pl will be.

=item B<-O|--format|--output-format> I<format>

Set the format used for results.
Valid values are: "plain", "stanza", "passwd", "html".
"stanza" is the default. For a description of these formats
see the documentation on Susi::ClientQuery.

=item B<-o|--output-file> I<output_file>

The file in which the results will be stored. 
If omitted results will be printed to stdout.

=back

=head1 RCM Options

=over 4

=item B<-u|--user> I<rcm-user>

Database account to connect to the rcm database. Only necessary if different from user stored in your personal defaults.

=item B<-p|--password> I<password>

Depending on your personal defaults a stored password is used automatically. If no stored password is available you will be prompted for it.

You will also prompted for a password if you are connection to a user different from the defaults (by means of --user).

If you want to prevent that behavior can you provide the password on the command line with option --password.

NOTE: the password needs to be given as plain text and might be visible in the process list.

=item B<-S|--server> I<database-server>

hostname of the machine, that runs the rcm database. Useful to connect other machines than the productive database.

=back

=head1 EXAMPLE

clone_sh_services.pl -C gsn -id 61029,12345,23456 --mode insert --target dbkprcm --debug

=head1 AUTHOR

S<Frank-Christian Otto>

S<(C) Copyright IBM Corporation 2000,2004>

=cut

# ------------------------------------------------------------------------- #
#  end POD
# ------------------------------------------------------------------------- #
