=pod
do_query() {
    query = "$@" query_result = $( $DBGETVAL -rc $DBGETVALRC "$@" ) RC = $?

      # exit 1 if RCM query failed !!!
      if [ [ "$RC" - ne 0 ] ];
    then log_entry ERROR $0 "RCM query '$*' failed. Should abort." echo
      "update_hosts_allow script return code is: 1" >
      $ERRORFILE fi echo "$query_result"
}
=cut
