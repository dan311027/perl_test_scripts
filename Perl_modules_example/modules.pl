#A module in Perl is a collection of related subroutines and variables that perform a set of programming tasks
#A modules name must be same as to the name of the Package and should end with .pm extension

package Calculator;

# Defining sub-routine for Multiplication
sub multiplication {

    # Initializing Variables a & b
    $a = $_[0];
    $b = $_[1];

    # Performing the operation
    $a = $a * $b;

    # Function to print the Sum
    print "\n***Multiplication is $a";
}

# Defining sub-routine for Division
sub division {

    # Initializing Variables a & b
    $a = $_[0];
    $b = $_[1];

    # Performing the operation
    $a = $a / $b;

    # Function to print the answer
    print "\n***Division is $a";
}
1;

#To import this calculator module, we use require or use functions. To access a function or a variable from a module, :: is used.

#!/usr/bin/perl

# Using the Package 'Calculator'
use Calculator;

print "Enter two numbers to multiply";

# Defining values to the variables
$a = 5;
$b = 10;

# Subroutine call
Calculator::multiplication( $a, $b );

print "\nEnter two numbers to divide";

# Defining values to the variables
$a = 45;
$b = 5;

# Subroutine call
Calculator::division( $a, $b );

#Using Variables from modules
#Variables from different packages can be used by declaring them before using .

#!/usr/bin/perl

package Message;

# Variable Creation
$username;

# Defining subroutine
sub Hello {
    print "Hello $username\n";
}
1;

#Using Pre-defined Modules
use strict;
use warnings;

print " Hello This program uses Pre-defined Modules";

##perl packages


