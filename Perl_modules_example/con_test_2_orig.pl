#!/usr/bin/perl
=pod
​
=head1 NAME
​
conn.pl
​
=head1 SYNOPSIS
​
conn.pl  -h <host> [ -u <user> -r <slsrelayuser> -s <sls> -d|--direct -c <remotecmd> --lopr <loprfile_systems_in_total> --[no]tmux ]
​
=cut
use v5.10;
use warnings;
use strict;
use Getopt::Long;
use FindBin qw($RealBin);
use IPC::Cmd qw(run);
use IO::File;
use File::Slurp;
use File::Spec::Functions qw(catfile catdir);
use Pod::Usage;
my $user = 'unxxx4';
my $host;
my $domain   = 'rze.de.db.com';
my $sls      = 'sls.rze.de.db.com';
my $slsuser  = 'sls4unx4';
my $slscmd   = 'sls -c';
my $sessions = catdir( $ENV{"HOME"}, "ssh_sessions" );
my $remotecmd;
my $direct = 0;
my $tmux;
my $lopr = "/Users/martin/work/lopr/rcm.systems_in_total";
# get the arguments
my $dummy = GetOptions(
    "h|host=s"    => \$host,
    "u|user=s"    => \$user,
    "s|sls=s"     => \$sls,
    "r|slsuser=s" => \$slsuser,
    "d|direct"    => \$direct,
    "c|cmd=s"     => \$remotecmd,
    "tmux!"       => \$tmux,
    "lopr=s"      => \$lopr,
) or pod2usage( verbose => 1 );
unless ( defined $host ) {
    pod2usage( verbose => 1 );
}
# domain parsing
if ( $host =~ m/\A(\w+)\.(.+)\z/ ) {
    $domain = $2;
    $host   = $1;
}
my $buffer;
my $cmd = "ssh -A -tt $slsuser\@$sls $slscmd $user\@$host";
if ($remotecmd) {
    $cmd = $cmd . " \\\"$remotecmd\\\"";
}
if ($direct) {
    $cmd = "ssh -A -t $user\@$host.$domain";
    if ($remotecmd) {
        $cmd = $cmd . " \"$remotecmd\"";
    }
}
# tmux rename window
if ( $tmux and $lopr ) {
    my @scope = read_file($lopr);
    my $line  = ( grep( /$host/, @scope ) )[0];
    if ($line) {
        chomp $line;
        my $os      = ( split /:/, $line )[5];
        my $status  = ( split /:/, $line )[8];
        my $tmuxstr = "tmux rename-window \"$host ($os) - $status\"";
        run( command => $tmuxstr );
    }
}
my $ok = run( command => $cmd, verbose => 1, buffer => \$buffer );
# write down the session log
if ($ok) {
    my $sessionlog = catfile( $sessions, $host . '_' . time() );
    if ( !-d $sessions ) {
        mkdir $sessions or die "Cannot create $sessions!";
    }
    my $fh = IO::File->new( $sessionlog, 'w' )
      or die "Cannot write into $sessionlog";
    print $fh $buffer;
    $fh->close();
    if ( $tmux and $lopr ) {
        run( command => "tmux rename-window shell" );
    }
}
