#if
#if (condition) {

    # code to be executed
#}
=cut
# Perl program to illustrate if statement
$a = 10;

# if condition to check
# for even number
if ( $a % 2 == 0 ) {
    printf "Even Number";
}

#if-else
# Perl program to illustrate
# if - else statement
$a = 21;

# if condition to check
# for even number
if ( $a % 2 == 0 ) {
    printf "Even Number";
}
else {
    printf "Odd Number\n";
}

#Nested – if Statement
#if statement inside an if statement is known as nested if. 
#if statement in this case is the target of another if or else statement. 
#When more then one condition needs to be true and one of the condition is the sub-condition of parent condition, nested if can be used

# Nested if statement
$a = 10;

if ( $a % 2 == 0 ) {

    # Nested - if statement
    # Will only be executed
    # if above if statement
    # is true
    if ( $a % 5 == 0 ) {
        printf "Number is divisible by 2 and 5\n";
    }
}

#If – elsif – else ladder Statement
$i = 20;

if ( $i == 10 ) {
    printf "i is 10\n";
}

elsif ( $i == 15 ) {
    printf "i is 15\n";
}

elsif ( $i == 20 ) {
    printf "i is 20\n";
}

else {
    printf "i is not present\n";
}


#unless Statement
# Perl program to illustrate
# unless statement

$a = 10;

unless ( $a != 10 ) {

    # if condition is false then
    # print the following
    printf "a is not equal to 10\n";
}

#Unless-else Statement
#Unless statement can be followed by an optional else statement, which executes when the boolean expression is true

$a = 10;

unless ( $a == 10 ) {

    # if condition is false then
    # print the following
    printf "a is not equal to 10\n";
}

else {

    # if condition is true then
    # print the following
    printf "a is equal to 10\n";
}

#Unless – elsif Statement
#Unless statement can be followed by an optional elsif…else statement
#which is very useful to test the various conditions using single unless…elsif statement
$a = 50;

unless ( $a == 60 ) {

    # if condition is false
    printf "a is not equal to 60\n";
}
elsif ( $a == 60 ) {

    # if condition is true
    printf "a is equal to 60\n";
}
else {

    # if none of the condition matches
    printf "The value of a is $a\n";
}

#