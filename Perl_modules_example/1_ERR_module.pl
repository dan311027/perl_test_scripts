=pod
from ksh script for deleting a hanging file

if [ [ -z $CONFIG_DEF ] ]     >> based on "/usr/db/local/make_environ.sh" is no more used
then . /usr/db/local/make_environ.sh
fi
[[ -n $FULLDEBUG ]] && FULLDEBUG="set -x"
$FULLDEBUG

nodename=$(hostname)
DATE=$(date +%Y%m%d.%H%M)

# use ERRORFILE to circumvent inability to do exit() from function
ERRORFILE=/var/db/work/update_hosts_allow

  #cleanup
  rm $ERRORFILE 2 > /dev/null do_query()
  {
    query = "$@" query_result = $( $DBGETVAL -rc $DBGETVALRC "$@" ) RC = $?

      # exit 1 if RCM query failed !!!
      if [ [ "$RC" - ne 0 ] ];
    then log_entry ERROR $0 "RCM query '$*' failed. Should abort." echo
      "update_hosts_allow script return code is: 1" >
      $ERRORFILE fi echo "$query_result"
}
=cut
# deleting a hanging file
use warnings;
use strict;
#ERRORFILE > based on sh scr ERRFILE need to be removed
my $ERRORFILE = "/tmp/update_hosts_allow";
my $removed = unlink($ERRORFILE);

print "Removed $removed file(s).\n";


