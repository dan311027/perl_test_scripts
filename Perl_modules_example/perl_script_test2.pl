#!/bin/zsh

# Perl Code to demonstrate the boolean values

# variable assigned value 0
$k = 0;
# checking whether k is true or false
if ($k)
{
	print "k is True\n";
}
else
{
	print "k is False\n";
}
=pod
# variable assigned value 2

$m = 2;
# checking whether m is true or false
if ($m)
{
	print "m is True\n";
}
else
{
	print "m is False\n";
}

#vari with string
$x = "GFG";
#string statement
if  ($x eq "GFG")
{
     print "Return True\n";
}
else
{
     print "Return False\n";
}

# Operators

q{Sg};
print "Value of g is = $r\n"

$r = qq{$g};

print


# Perl program to demonstrate
# character class
	
# Actual String
$str = "45char=";
	
# Prints match found if
# its found in $str
# by using \w
if ($str =~ /[\W]/)
{
	print "Match Found\n";
}
	
# Prints match not found
# if it is not found in $str
else
{
	print "Match Not Found\n";
}

# Perl program to demonstrate
# use of anchors in regex

# Actual String
$str = "a5";
	
# Prints match found if
# its found in $str
# using Anchors /
if ($str =~ /[[:alpha:]]/)
{
	print "Match Found\n";
}
	
# Prints match not found
# if it is not found in $str
else
{
	print "Match Not Found\n";
}



# Perl program to demonstrate
# use of quantifiers in regex
	
# Actual String
$str = "color";
	
# Prints match found if
# its found in $str
# using quantifier ?
if ($str =~ /colou?r/)
{
	print "Match Found\n";
}
	
# Prints match not found
# if it is not found in $str
else
{
	print "Match Not Found\n";
}


=cut
##################################RCM Martin script
#!/usr/db/bin/perl
use warnings;
use strict;use lib qw(/usr/db/RCM/Classes);
use Architecture;
use Machine;
use Query;use Data::Dumper;my $hostid = Machine->hostid;my $key = ‘tcpd_perm_by_hostid’;
my $version = ‘1.0’;my $query = Query->new($key, $version);
my $results = $query->query( { hostid => $hostid } );foreach ($results->records) 
{
my $rec = Record->new($_);
   print $rec->getval(‘service’) . “:\t” . $rec->getval(‘client’) . “\n”;
}
=cut
