use warnings;
use strict;
#use say;

use POSIX ":sys_wait_h";
my $exited_cleanly;    #to this variable I will save the info about exiting
my $cmd = `w`;         #w trigerr the program you want
my $pid = fork;
if ( !$pid ) {
    system($cmd);    #your long program
}
else {
    sleep 10;                               #wait 10 seconds (can be longer)
    my $result = waitpid( -1, WNOHANG );    #here will be the result

    if ( $result == 0 ) {                   #system is still running
        $exited_cleanly = 0;                #I already know I had to kill it
        kill( 'TERM', $pid );               #kill it with TERM ("cleaner") first
        sleep(1);                           #wait a bit if it ends
        my $result_term = waitpid( -1, WNOHANG );

        #did it end?

        if ( $result_term == 0 ) {          #if it still didnt...
            kill( 'KILL', $pid );           #kill it with full force!
        }
    }
    else {
        $exited_cleanly = 1;                #it exited cleanly
    }
}

#################################################
