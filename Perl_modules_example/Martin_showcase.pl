#!/bin/perl
###############
use warnings;
use strict;
use lib qw(/usr/db/RCM/Classes);
#use Architecture;
#use Machine;
#use Query;
#use bin; 
use Data::Dumper;
#use Record;

my $hostid = Machine->hostid;
my $key = `db_sw_config_table`;
my $version = `1.0`;
my $query = Query->new($key, $version);
my $results = $query->query( { hostid => $hostid } );

foreach ($results->records) 
{
my $rec = Record->new($_);
print $rec->getval('service') . ":\t" . $rec->getval('client') . "\n";
   }
